const chai = require('chai');
const expect = chai.expect;

//same as const { expect } = require("chai")

const http = require('chai-http');
chai.use(http);

describe("api_test_suite", () => {
	it("test_api_get_people_is_running", () => {
		// .request() is a chai method that accepts the server url
		chai.request('http://localhost:5001').get('/people')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})

	it('test_api_get_people_returns_200', (done) => {
		chai.request('http://localhost:5001')
		//.get() specifies the type of http request as GET and accepts the API endpoint
		.get('/people')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})

	it('test_api_post_person_returns_400_if_no_person_name', (done) => {		
		chai.request('http://localhost:5001')
		//.post() specifies the type of http request as POST
		.post('/person')
		// .type() specifies the type of input to be sent out as part of the POST request
		.type('json')
		// .send() specifies the data to be sent as part of the POST request
		.send({
		    alias: "Jason",
	      	age: 28
		})
		// .send() specifies the data to be sent as part of the POST request
		.end((err, res) => {
			expect(res.status).to.equal(400);
			// .done() states that the test is currently done
			done();	
		})		
	})

	it("test_api_post_person_is_running", () => {
		chai.request('http://localhost:5001')
		.post('/people')
		.type('json')
		.send({
			alias: "Jay",
			name: "Jay White",
			age: 27
		})
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})

	it('test_api_post_person_returns_400_if_no_ALIAS', () => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			name: "Jay White",
			age: 27
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
		})
	})

	it('test_api_post_person_returns_400_if_no_AGE', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "Jay",
			name: "Jay White",
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('test_api_post_person_returns_400_if_no_AGE', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "Jay",
			name: "Jay White"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})

		it('test_api_post_login_returns_400_if_no_username', (done) => {
		chai.request('http://localhost:5001')
		.post('/login')
		.type('json')
		.send({
			password: "wrongPW"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('test_api_post_login_returns_400_if_no_password', (done) => {
		chai.request('http://localhost:5001')
		.post('/login')
		.type('json')
		.send({
			username: "brBoyd87"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done();
		})
	})

	it('test_api_post_login_returns_200_if_correct_credentials', (done) => {
		chai.request('http://localhost:5001')
		.post('/login')
		.type('json')
		.send({
			username: "brBoyd87",
			password: "87brandon19"
		})
		.end((err, res) => {
			expect(res.status).to.equal(200)
			done();
		})
	})

	it('stretch_goal_post_login_returns_403_if_wrong_credentials', (done) => {
		chai.request('http://localhost:5001')
		.post('/login')
		.type('json')
		.send({
			username: "brBoyd87",
			password: "87brandon1999",
		})
		.end((err, res) => {
			expect(res.status).to.equal(403)
			done();
		})
	})



})
