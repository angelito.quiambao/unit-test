const { factorial, div_check } = require('../src/util.js');

// required for the assertion to be tested.
const { expect, assert } = require('chai');

describe('test_fun_factorials', ()=> {
	// it() accepts two parameters
	// a string explaining what the test should do, 
	// and a callback function which contains the actual test

	//Expect
		// The expect module uses chainable language to construct assertions.
		// Continues after the failure.
	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect(product).to.equal(120, "product should be equal to 120");
	});

	// Assert
		// The assert uses the assert-dot notation that node.js has.
		// The assert module also provides several additional tests.
		// Fails fast, aborting the current function.
	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		// accepts two parameters
			// 1st parameter - actual value
			// 2nd parameter - expected value
			// 3rd parameter (optional) - message to display if tests fails
		assert.equal(product, 1, "product should be equal to 1");
	});

	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial(0)
		assert.equal(product, 1)
	});

	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4)
		expect(product).to.equal(24)
	});

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10)
		expect(product).to.equal(3628800)
	});

});

describe('test_divisibilty_by_5_or_7', ()=>{

	it('test_100_is_divisible_by_5', () => {
		const number = div_check(100);
		expect(number).to.equal(true);
	})

	it('test_49_is_divisible_by_7', () => {
		const number = div_check(49);
		expect(number).to.equal(true);
	});


	it('test_30_is_divisible_by_5', () => {
		const number = div_check(30);
		expect(number).to.equal(true);
	});

	it('test_56_is_divisible_by_7', () => {
		const number = div_check(56);
		expect(number).to.equal(true);
	});	

});

